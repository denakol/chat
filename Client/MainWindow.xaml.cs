﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Helpers;


namespace Client
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    /// 
    /// 
    public delegate void MyDelegate(StartInformation startInformation);
    public delegate void DelegateOpenChat(ClientEntity clientEntity);
    public delegate void DelegateSendAgreeChat(AgreeChat agreeChat);

    public partial class MainWindow : Window
    {
        private static TcpClient client;
        public static List<string> _message;
        private static object locker = new object();
        private static event EventHandler MyHandle;
        private static event EventHandler AgreeWindow;
        private static event EventHandler UpdateUserListHandle;
        private static IPEndPoint _ipEndPoint;
        private static String _login;
        public MainWindow()
        {
            InitializeComponent();
            _message = new List<string>();
            client = new TcpClient();
            var window = new Startup( new MyDelegate(SetEndPoint));
            window.ShowDialog();
            MyHandle += new EventHandler(NewMessage);
            AgreeWindow += AgreedChat;
            UpdateUserListHandle += new EventHandler(UpdateUserList);
            var thread = new Thread(ClientСycle) { IsBackground = true };
            thread.Start();
            var dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0,2);
            dispatcherTimer.Start();
        }
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            var stream = client.GetStream();
            var command = new Command() { typeCommand = TypeCommand.UserList };
            var comand = (new MySerizable<Command>()).SerializeUserList(command);
            stream.Write(BitConverter.GetBytes(comand.Length), 0, 4);
            stream.Write(comand, 0, comand.Length);
        }
        private void SetEndPoint(StartInformation startInformation)
        {
            _ipEndPoint = startInformation.EndPoint;
            _login = startInformation.Login;
        }
        private void NewMessage(object sender, EventArgs e)
        {
            lock (locker)
            {
                if (_message.Count > 0)
                {
                    History.Text += _message[0];
                    _message.RemoveAll(x => true);
                }
            }

        }
        private void UpdateUserList(object sender, EventArgs e)
        {
            var userList = (UserList) sender;
            lbContactList.ItemsSource = userList.userList;
        }
        private void ClientСycle()
        {
                client.Connect(_ipEndPoint);
                Login();
                var stream = client.GetStream();
                while (true)
                {
                   
                    {
                        var buffer = new byte[32768];
                        stream.Read(buffer, 0, 4);
                        int length = BitConverter.ToInt32(buffer, 0);
                        if (length > 0 && length <= 32768)
                        {
                            stream.Read(buffer, 0, length);
                            FileInfo Log = new FileInfo("stat.txt");
                            StreamWriter LogWriter;
                            LogWriter = Log.AppendText();
                            LogWriter.WriteLine(length+4 + "  "+ DateTime.Now.ToString());
                            LogWriter.Close();
                        }
                        else
                        {
                            throw new Exception();
                        }
                        var command = (Command) (new MySerizable<Command>()).DeserializeUserList(buffer);
                        switch (command.typeCommand)
                        {
                            case TypeCommand.Message:
                                ReadNewMassage(command);
                                break;
                            case TypeCommand.UserList:
                                GetUserList(command);
                                break;
                              case TypeCommand.CreateChat:
                                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, AgreeWindow, command,
                                                               EventArgs.Empty);
                                break;
                              case TypeCommand.CreatedChat:
                                RunOnlyChat(command);
                                break;


                        }

                    }

                }
            

        }
        private static void AgreedChat(object sender, EventArgs e)
        {
            var window = new CreateChatWindow(SendAgreedChat, (CreateChat)sender);
            window.Show();
        }
        private async  static void RunOnlyChat(Command command)
        {
            var createdChat = (CreatedChat) command;
            var tcpClient = new TcpClient();
            if (createdChat.Inic)
            {
                var listener = new TcpListener(IPAddress.Any, createdChat.EndPoint.Port);
                        listener.Start();
                     tcpClient=listener.AcceptTcpClient();
              
            }
            else
            {
                await tcpClient.ConnectAsync(createdChat.EndPoint.Address, createdChat.EndPoint.Port);
            }
            var clientEntity = new ClientEntity() {Client = tcpClient, Login = _login};
            DelegateOpenChat openCat = OpenChat;
            Application.Current.Dispatcher.BeginInvoke(openCat,clientEntity);

        }
        private static void OpenChat(ClientEntity cliententity)
        {
            var window = new ChatClientToClientWindow(cliententity);
            window.Show();
            
        }
        private  static  void SendAgreedChat(AgreeChat agreeChat)
        {
            var stream = client.GetStream();
            var comand = (new MySerizable<AgreeChat>()).SerializeUserList(agreeChat);
            stream.Write(BitConverter.GetBytes(comand.Length), 0, 4);
            stream.Write(comand, 0, comand.Length);
        }
        private static void GetUserList(Command command)
        {
            var userList = ((UserList) command);
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, UpdateUserListHandle, userList,
                                                               EventArgs.Empty);
        }
        private static void Login()
        {
            var stream = client.GetStream();
            var login = new LoginOn() {Login = _login};
            var message = (new MySerizable<LoginOn>()).SerializeUserList(login);
            stream.Write(BitConverter.GetBytes(message.Length), 0, 4);
            stream.Write(message, 0, message.Length);
        }
        static void ReadNewMassage(Command command)
        {
            var message = (Message) command;
            FileInfo Log = new FileInfo("log.txt");
            StreamWriter LogWriter;
            LogWriter = Log.AppendText();
            LogWriter.WriteLine(message.TextMessage + DateTime.Now.ToString());
            LogWriter.Close();
                    lock (locker)
                    {
                       _message.Add(message.TextMessage);
                    }
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, MyHandle, 1,
                                                               EventArgs.Empty);
                
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
           var stream = client.GetStream();
           var command = new Message() {Receiver = _login, TextMessage =txbNewMessage.Text , typeCommand = TypeCommand.Message};
           var comand = (new MySerizable<Message>()).SerializeUserList(command);
           stream.Write(BitConverter.GetBytes(comand.Length), 0, 4);
           stream.Write(comand, 0, comand.Length);
           txbNewMessage.Clear();
        }
        private void lbContactList_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var stream = client.GetStream();
            var command = new CreateChat()
                {
                    typeCommand = TypeCommand.CreateChat,
                    Login = (String) lbContactList.Items.CurrentItem
                };
            var comand = (new MySerizable<CreateChat>()).SerializeUserList(command);
            stream.Write(BitConverter.GetBytes(comand.Length), 0, 4);
            stream.Write(comand, 0, comand.Length);
        }
    }
}
