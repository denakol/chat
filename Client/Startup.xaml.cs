﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Helpers;

namespace Client
{
    /// <summary>
    /// Логика взаимодействия для Startup.xaml
    /// </summary>
    public partial class Startup : Window
    {
        private MyDelegate d;
        public Startup( MyDelegate setIp)
        {
            InitializeComponent();
            d = setIp;

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var ip = IPAddress.Parse(ipAdres.Text);
                var portInt = Convert.ToInt32(Port.Text);
                var endPoint = new IPEndPoint(ip, portInt);

                var login = txLogin.Text;
                var start = new StartInformation() {EndPoint = endPoint, Login = login};
                d(start);
                Close();
            }
            catch (Exception)
            {
                
                throw;
            }
            
        }


    }
}
