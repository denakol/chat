﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Helpers;
using Microsoft.Win32;

namespace Client
{
    /// <summary>
    /// Логика взаимодействия для ChatClientToClientWindow.xaml
    /// </summary>
    public delegate void UpdateProgressBarDelegateWindow(System.Windows.DependencyProperty dp, Object value);

    public partial class ChatClientToClientWindow : Window
    {
        private ChatClientToClient chatClient;
        private UpdateProgressBarDelegateWindow updatePbDelegate;
        private double value = 0;
        public ChatClientToClientWindow(ClientEntity clientEntity)
        {
               InitializeComponent();
               updatePbDelegate =new UpdateProgressBarDelegateWindow(progressBar.SetValue);
                chatClient = new ChatClientToClient(clientEntity, UpdateProgressBar);
                chatClient.NewMessage += NewMessage;
                chatClient.NewFile += NewFileSend;
                chatClient.ExceptionMessage += ExceptionMEssage;
            

        }
        private void ExceptionMEssage(object sender, EventArgs e)
        {
            MessageBox.Show("Пользователь отключился");
        }
        private void UpdateProgressBar()
        {
           Dispatcher.Invoke(updatePbDelegate,System.Windows.Threading.DispatcherPriority.Background,
              new object[] { ProgressBar.ValueProperty,++value  });
        }
        private void NewMessage(object sender, EventArgs e)
        {
            var message = (Message) sender;
            txbChat.Text += String.Format("{0} : {1}\n", message.Sender, message.TextMessage);
        }
        private void NewFileSend(object sender, EventArgs e)
        {
            var name = String.Format("Вам прислан файл\n {0}",(String) sender);
            MessageBox.Show(name);
        }
        private void Send_Click(object sender, RoutedEventArgs e)
        {
            chatClient.SendMessage(txbNewMessage.Text);
            txbNewMessage.Clear();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Stream myStream = null;
            var openFileDialog1 = new OpenFileDialog();
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.ShowDialog();
            myStream = openFileDialog1.OpenFile();
            try
            {
                progressBar.Maximum = myStream.Length/32768;
                chatClient.SendFile(myStream, openFileDialog1.SafeFileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
            }
            finally
            {
                progressBar.Value = 0;
            }
            
            
        }

     
    }
}
