﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Helpers;

namespace Client
{
    /// <summary>
    /// Логика взаимодействия для CreateChatWindow.xaml
    /// </summary>
    public partial class CreateChatWindow : Window
    {
        private DelegateSendAgreeChat agreeChat;
        private CreateChat _createChat;
        public CreateChatWindow(DelegateSendAgreeChat dAgree, CreateChat createChat)
        {
            InitializeComponent();
            _createChat = createChat;
            agreeChat = dAgree;
            lbLogin.Content += String.Format("В согласны начать чат с {0}", createChat.Login);
        }

        private void Yes_Click(object sender, RoutedEventArgs e)
        {
            var agree = new AgreeChat() {Login = _createChat.Login, typeCommand = TypeCommand.AgreeChat, Yes = true};
            agreeChat(agree);
            Close();
        }

        private void No_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
