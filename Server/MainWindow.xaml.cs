﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Helpers;

namespace Server
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
           var ipAd = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipv4 = ipAd.AddressList.First(x => x.AddressFamily == AddressFamily.InterNetwork);
            Adres.Text = ipv4.ToString();
            Adres.Text += ": 3000";
            var listener = new TcpListener(IPAddress.Any, 3000);
            var server = new ServerChat(listener);
            server.Start();
               
        }

    }
}
