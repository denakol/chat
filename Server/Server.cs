﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Helpers;

namespace Server
{
    class ServerChat
    {
        private static List<ClientEntity> _clients;
        private readonly TcpListener _listener;
        private static int Port=4000;
        private static object Locker = new object();
        public ServerChat(TcpListener listener)
        {
            _listener = listener;
            _clients = new List<ClientEntity>();
        }
        public void  Start()
        {
            _listener.Start();
            var thread = new Thread(Listener) { IsBackground = true };
            thread.Start(_listener);
        }
        static void Listener(object arg)
        {
            var listener = (TcpListener)arg;
        
            while (true)
            {
                var client = listener.AcceptTcpClient();
                var login = GetLogin(client);
                var clientEntity = new ClientEntity() {Client = client, Login = login};
                _clients.Add(clientEntity);
                ThreadPool.QueueUserWorkItem(new WaitCallback(ClientThread), clientEntity);
            }

        }
        private static void ClientThread(Object StateInfo)
        {
            
            var  client = (ClientEntity)StateInfo;
            try
            {
                while (true)
                {
                    var stream = client.Client.GetStream();
                    {
                        byte[] buffer = new byte[32768];
                        stream.Read(buffer, 0, 4);
                        int length = BitConverter.ToInt32(buffer, 0);
                        if (length > 0 && length <= 32768)
                        {
                            stream.Read(buffer, 0, length);
                        }
                        else
                        {
                            throw new Exception();
                        }
                        var command = (Command)(new MySerizable<Command>()).DeserializeUserList(buffer);
                        switch (command.typeCommand)
                        {
                            case TypeCommand.Message:
                                NewMessege(client,command);
                                break;
                            case TypeCommand.UserList:
                                GetUserList(client);
                                break;
                            case TypeCommand.CreateChat:
                                SendCreateChat(client, command);
                                break;
                            case TypeCommand.AgreeChat:
                                SendCreatedChat(client, command);
                                break;
                        }
                    }
                }
            }
            catch (IOException ex)
            {
                client.Client.Close();
                _clients.Remove(client);
            }
        }
        private static void SendCreatedChat(ClientEntity client, Command command)
        {
            var login = ((AgreeChat)command).Login;
            var creator = _clients.First(x => x.Login == login);
            var stream = creator.Client.GetStream();
            var endPoint = (IPEndPoint) client.Client.Client.RemoteEndPoint;
            endPoint.Port = Port;
            var createdChatSendCreator = new CreatedChat()
                {
                    EndPoint = endPoint,
                    Inic = true,
                    Login = client.Login,
                    typeCommand = TypeCommand.CreatedChat
                };
            var sendBytes = (new MySerizable<CreatedChat>()).SerializeUserList(createdChatSendCreator);
            stream.Write(BitConverter.GetBytes(sendBytes.Length), 0, 4);
            stream.Write(sendBytes, 0, sendBytes.Length);

            var companion = client;
            stream = companion.Client.GetStream();
            endPoint = (IPEndPoint)creator.Client.Client.RemoteEndPoint;
            endPoint.Port = Port;
            var createdChatSendCompanion = new CreatedChat()
            {
                EndPoint = endPoint,
                Inic = false,
                Login = creator.Login,
                typeCommand = TypeCommand.CreatedChat
            };
            sendBytes = (new MySerizable<CreatedChat>()).SerializeUserList(createdChatSendCompanion);
            stream.Write(BitConverter.GetBytes(sendBytes.Length), 0, 4);
            stream.Write(sendBytes, 0, sendBytes.Length);
            Interlocked.Increment(ref Port);


        }
        private static void GetUserList(ClientEntity client)
        {
            var stream = client.Client.GetStream();
            var users = _clients.Where(x=>x.Login!=client.Login).Select(x => new string(x.Login.ToCharArray()));
            var userList = new UserList() {typeCommand = TypeCommand.UserList, userList = users.ToList()};
            var message = (new MySerizable<UserList>()).SerializeUserList(userList);
            stream.Write(BitConverter.GetBytes(message.Length), 0, 4);
            stream.Write(message, 0, message.Length); 
        }
        private static String GetLogin(TcpClient client)
        {
            var stream = client.GetStream();
            var buffer = new byte[1024];
            stream.Read(buffer, 0, 4);
            int length = BitConverter.ToInt32(buffer, 0);
            if (length > 0 && length <= 1024)
            {
                stream.Read(buffer, 0, length);
            }
            else
            {
                throw new Exception();
            }
            var login = (new MySerizable<LoginOn>()).DeserializeUserList(buffer);
            return login.Login;
        }
        private static void NewMessege(ClientEntity client, Command command)
        {
            try
            {
                var message = (Message) command;
                var stream = client.Client.GetStream();
                {
                    var messageWithAuthor = String.Format("{0} : {1} \n", client.Login,message.TextMessage );
                    foreach (var tcpClient in _clients)
                    {
                        SendMessage(tcpClient, messageWithAuthor);
                    }
                }
            }
            catch (IOException ex)
            {
                client.Client.Close();
                _clients.Remove(client);
            }
        }
        private static void SendMessage(ClientEntity client, String message)
        {
            try
            {
              //  var IpClient = ((IPEndPoint)client.Client.Client.LocalEndPoint).Address;
                var stream = client.Client.GetStream();
                {
                    var command = new Message() { Receiver = client.Login, TextMessage = message, typeCommand = TypeCommand.Message };
                    var comand = (new MySerizable<Message>()).SerializeUserList(command);
                    stream.Write(BitConverter.GetBytes(comand.Length), 0, 4);
                    stream.Write(comand, 0, comand.Length);
                }
            }
            catch (IOException ex)
            {
                client.Client.Close();
                _clients.Remove(client);
            }
        }
        private static void SendCreateChat(ClientEntity client, Command command)
        {
            var login = ((CreateChat) command).Login;
            var receiver = _clients.First(x => x.Login == login);
            var stream = receiver.Client.GetStream();
            var createChat = new CreateChat()
                {
                   typeCommand = TypeCommand.CreateChat,
                   Login = client.Login
                };
            var sendBytes = (new MySerizable<CreateChat>()).SerializeUserList(createChat);
            stream.Write(BitConverter.GetBytes(sendBytes.Length), 0, 4);
            stream.Write(sendBytes, 0, sendBytes.Length);
        }
        private static void SendFile(ClientEntity client)
        {


        }
    }

 
}
