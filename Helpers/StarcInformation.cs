﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Helpers
{

    public class StartInformation
    {
        public String Login { get; set; }
        public IPEndPoint EndPoint { get; set; }
        public StartInformation()
        {}
    }




    public static class Encryptor
    {
        public static string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            //compute hash from the bytes of text
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

            //get hash result after compute it
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits
                //for each byte
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }
    }
    [Serializable]
    public enum TypeCommand
    {
        Message,
        FileHandle,
        Login,
        UserList,
        Online,
        Register,
        CreateChat,
        AgreeChat,
        CreatedChat,

    }
    [Serializable]
    public class Command
    {
        public TypeCommand typeCommand { get; set; }
    }
    [Serializable]
    public class FileHandle : Command
    {
        public string NameFile ;
    }
    [Serializable]
    public class Message : Command
    {
        public String TextMessage { get; set; }
        public String Receiver { get; set; }
        public String Sender { get; set; }
    }
    [Serializable]
    public class LoginOn : Command
    {
        public String Login { get ; set; }
    }
    [Serializable]
    public class UserList : Command
    {
        public List<String> userList { get; set; }
    }
    [Serializable]
    public class CreateChat : Command
    {
        public String Login { get; set; }
    }

    [Serializable]
    public class AgreeChat :Command
    {
        public bool Yes { get; set; }
        public string Login { get; set; }
    }

    [Serializable]
    public class CreatedChat : Command
    {
        public IPEndPoint EndPoint { get; set; }
        public bool Inic { get; set; }
        public String Login { get; set; }
        public int Port;
    }

 
}
