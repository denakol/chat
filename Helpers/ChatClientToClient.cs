﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Helpers
{
    public delegate void UpdateProgressBarDelegate();
    public class ChatClientToClient
    {
        private static ClientEntity _client;
        public event EventHandler NewMessage;
        public event EventHandler NewFile;
        public event EventHandler ExceptionMessage;
        public event EventHandler UpdateProgressBar;
        private UpdateProgressBarDelegate updateProgressBar;
        private ManualResetEvent DowlandAccept = new ManualResetEvent(false);
        public bool StopSend=false;
        public object Locker = new object(); 
        public ChatClientToClient(ClientEntity client, UpdateProgressBarDelegate updateProgress)
        {
            _client = client;

            updateProgressBar = updateProgress;
            var thread = new Thread(ClientCycle) {IsBackground = true};
            thread.Start();
        }
        public void SendFile(Stream streamFile, String name)
        {
                StopSend = true;
                var stream = _client.Client.GetStream();
                var command = new FileHandle() {NameFile = name, typeCommand = TypeCommand.FileHandle};
                var comand = (new MySerizable<FileHandle>()).SerializeUserList(command);
                stream.Write(BitConverter.GetBytes(comand.Length), 0, 4);
                stream.Write(comand, 0, comand.Length);
                stream.Write(BitConverter.GetBytes(streamFile.Length), 0, 8);
                byte[] buffer = new byte[32768];
                while (streamFile.Position < streamFile.Length)
                {
                    int count = streamFile.Read(buffer, 0, 32768);
                    stream.Write(buffer, 0, count);
                    updateProgressBar();
                }
                StopSend = false;


        }
        public void SendMessage(String textMessage)
        {
            if (!StopSend)
            {
                var stream = _client.Client.GetStream();
                var command = new Message()
                    {
                        Sender = _client.Login,
                        TextMessage = textMessage,
                        typeCommand = TypeCommand.Message
                    };
                var comand = (new MySerizable<Message>()).SerializeUserList(command);
                stream.Write(BitConverter.GetBytes(comand.Length), 0, 4);
                stream.Write(comand, 0, comand.Length);
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, NewMessage, command,
                                                           EventArgs.Empty);
            }
        }
        public void DowlandFile(Command command)
        {
          
                StopSend = true;
                var stream = _client.Client.GetStream();
                var buffer = new byte[32768];
                var name = ((FileHandle) command).NameFile;
                stream.Read(buffer, 0, 8);
                long length = BitConverter.ToInt64(buffer, 0);
                using (var fileStream = File.Create(Path.GetFileName(name)))
                {
                    int count;
                    for (long sum = 0; sum < length; sum += count)
                    {
                         count= stream.Read(buffer, 0, 32768);
                        fileStream.Write(buffer, 0, count);
                    }
                }
                StopSend = false;
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, NewFile, name,
                                                               EventArgs.Empty);
         
        }

        public void ClientCycle()
        {
            try
            {
                var stream = _client.Client.GetStream();
                while (true)
                {

                    {
                        var buffer = new byte[32768];

                        stream.Read(buffer, 0, 4);

                        int length = BitConverter.ToInt32(buffer, 0);
                        if (length > 0 && length <= 32768)
                        {
                            stream.Read(buffer, 0, length);
                        }
                        else
                        {
                            break;
                        }
                        var command = (Command) (new MySerizable<Command>()).DeserializeUserList(buffer);
                        switch (command.typeCommand)
                        {
                            case TypeCommand.Message:
                                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, NewMessage,
                                                                           command,
                                                                           EventArgs.Empty);
                                break;
                            case TypeCommand.FileHandle:
                                DowlandFile(command);
                                break;

                        }
                    }
                }
            }
            catch (IOException ex)
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, ExceptionMessage,
                                                                            1,
                                                                            EventArgs.Empty);
            }
            }
         


    }
    }

