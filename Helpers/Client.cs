﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Helpers
{
    [Serializable]
    public class ClientEntity
    {
        public TcpClient Client { get; set; }
        public String Login { get; set; }
    }
}
